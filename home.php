<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

	<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container" ">

			<div class="row">
				<div class="col-12">
					<h1 class="heading-feature--home">
						<?php if(get_field('page_title', 'options')): ?>
							<?php the_field('page_title', 'options') ?></h1>
						<?php else: ?>
							<span>Tolka</span>
							<span>Web D</span>
							<span>evelo</span>
							<span>pment</span>
						<?php endif ?>
					</h1>
					<p class="pb-md-10 pb-5"> <?php the_field('intro_text', 'options') ?></p>
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-md-4">
					<?php get_template_part( 'templates-sidebar/sidebar', 'posts' ); ?>
				</div>
				<div class="col-12 col-md-8">
					<h2 class="text-uppercase">All Posts</h2>
					<?php
					if ( have_posts() ) {
						// Start the Loop.
						while ( have_posts() ) {
							the_post();
							get_template_part( 'templates-loop/content', 'archive-news' );
						}
					} else {
						get_template_part( 'templates-loop/content', 'none' );
					}
				?>
				</div>
			</div>

			<div class="row">
				<div class="col-12 mb-9">
					<!-- The pagination component -->
					<?php tolka_pagination(); ?>
				</div>
			</div>


		</div>
	</main><!-- #main -->

<?php
get_footer();
