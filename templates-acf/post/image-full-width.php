<section class="section-post section-post--image-full py-md-8 py-3">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10 offset-md-1 ">
			<?php if(get_sub_field('image')) : ?>
				<?php $img = get_sub_field('image') ?>

					<?php if(get_sub_field("heading")):?>
						<h2><?php the_sub_field("heading") ?></h2>
					<?php endif ?>
					<figure>
					<img class="w-100" src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
					<?php if(get_sub_field("image_caption")):?>
						<figcaption><?php the_sub_field("image_caption") ?></figcaption>
					<?php endif ?>
					</figure>
			<?php endif ?>
			</div>
		</div>
	</div>
</section>

