<section class="section-post section-post--text-two-col py-md-8 py-3">
    <div class="container">
        <div class="row">
			<?php if(get_sub_field("heading")):?>
				<div class="col-12  offset-md-1">
					<h2><?php the_sub_field("heading") ?></h2>
				</div>
			<?php endif ?>

            <div class="col-12 col-md-5 offset-md-1">
			<?php if(get_sub_field("text_lhs")) : ?>
                <?php the_sub_field("text_lhs") ?>
			<?php endif; ?>
            </div>
            <div class="col-12 col-md-5">
				<?php if(get_sub_field("text_rhs")) : ?>
                    <?php the_sub_field("text_rhs") ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</section>

