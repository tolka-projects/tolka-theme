<section class="section-post section-post--text py-md-8 py-3">
<div class="container">
	<div class="row">
	<div class="col-12 col-md-10 offset-md-1">
			<?php if(get_sub_field("text")) : ?>
				<?php the_sub_field("text") ?>
			<?php endif ?>
		</div>
	</div>
	</div>
</section>

