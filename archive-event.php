<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();




global $post;
$current_time = current_time('Y-m-d');
$args = array(
	'posts_per_page' => 6,
	'post_type' => 'event',
	'orderby'			=> 'meta_value',
	'order'            => 'ASC',
	'meta_query' => array(
        array(
            'key'     => 'start_date',
            'value'   => $current_time ,
            'compare' => '>=',
            'type'    => 'DATE',
        ),
    ),
    'orderby' => '_event_start_date',
);

$event = new WP_Query($args);

?>

	<main class="site-main cpt-archive" id="main" role="main">
		<div class="container upcoming-events mb-4 pb-4 mb-md-8 pb-md-4">
			<div class="row">
				<div class="col-12">
					<h2 class="mb-6">Upcoming Events</h2>
				</div>
				<div class="col-12 p-0">
				<?php if ($event->have_posts()): ?>
					<div class="" >
						<?php while($event->have_posts()):

							$event->the_post();
							$event_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
							$author = get_the_author();
							$date = get_the_date( 'F Y');
							?>

							<div class="px-3">
								<article class="news-tile">
									<a href="<?php the_permalink(); ?>">

										<h3><?php the_title(); ?></h3>
										<p class="location mb-0"> <?php the_field('location') ?> </p>
										<p class="date mb-0"> <?php the_field('display_date') ?> </p>
									</a>
								</article>
							</div>

						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>


		</div><!-- #content -->
	</main><!-- #main -->
<?php
get_footer();
