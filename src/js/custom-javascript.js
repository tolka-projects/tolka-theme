document.getElementsByTagName('html')[0].classList.remove('no-js');

/************************************************************
FUNCTION: assignAnchorScrollEvents()
DESC:	    animated scroll to an elem if linked with an #anchor
************************************************************/
function assignAnchorScrollEvents() {
	var AnchorLinkElems = jQuery('a[href*="#"]');

	AnchorLinkElems.on('click', function(e) {
    var elem 		= jQuery(this);
    var target	= elem.attr('href');

    if (jQuery(target).length) {
      e.preventDefault();

       htmlBodyElem.stop().animate({
          scrollTop: jQuery(target).offset().top - 40
      }, 1800);
    }
	});
} //END assignAnchorScrollEvents()


jQuery( document ).ready( function( $ ) {

console.log("I see you!!!");

//assignAnchorScrollEvents();

/************************************************************
Search Modal
************************************************************/
    // $('#search-trigger').click(function(){
    //   $('#search-modal').css('visibility', 'visible');
    //   $('#search-modal').css('opacity', '.9');
    // })

    // $('#search-close').click(function(){
    //   $('#search-modal').css('visibility', 'hidden');
    // });

    // $('#accordian-trigger').on("click" , function(){
    //   console.log("clicked");
    //   $(this).toggleClass('opened');
    //   $('#accordian-body').toggleClass('open');
    // })

/************************************************************
Woocommerce like Gallery
************************************************************/
  // $('.image--thumbs').click(function(){
  //   var attVal = $(this).data('thumbindex');
  //   $('.image--gallery ').removeClass('active');
  //   // $('.image--gallery').data('galleryindex', attVal ).addClass('active');
  //   $('.image--gallery[data-galleryindex=' + attVal +']').addClass('active');
	// });


/************************************************************
Back to top
************************************************************/


$('#back-to-top').on('click', function () {
	document.body.scrollTop = 0; // For Safari
	document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
})

/************************************************************
Activate Bootstrap Nav on hover
************************************************************/

// const $dropdown = $(".dropdown");
// const $dropdownToggle = $(".dropdown-toggle");
// const $dropdownMenu = $(".dropdown-menu");
// const showClass = "show";

// $(window).on("load resize", function() {
//   if (this.matchMedia("(min-width: 1200px)").matches) {
//     $dropdown.hover(
//       function() {
//         const $this = $(this);
//         $this.addClass(showClass);
//         $this.find($dropdownToggle).attr("aria-expanded", "true");
//         $this.find($dropdownMenu).addClass(showClass);
//       },
//       function() {
//         const $this = $(this);
//         $this.removeClass(showClass);
//         $this.find($dropdownToggle).attr("aria-expanded", "false");
//         $this.find($dropdownMenu).removeClass(showClass);
//       }
//     );
//   } else {
//     $dropdown.off("mouseenter mouseleave");
//   }
// });

/************************************************************
Wrap iframes for responsiveness
************************************************************/

$('iframe').wrap('<div class="fluid-width-video-wrapper" />');


/************************************************************
Floating Nav on scroll
************************************************************/

// var waypoints = jQuery('#side-nav').waypoint(function(direction) {
// 	console.log("triggered");

// 	if(direction == 'down') {
// 		jQuery('#side-nav').addClass('side-nav-sticky');
// 		}
// 		if(direction == 'up') {
// 		jQuery('#side-nav').removeClass('side-nav-sticky');
// 		}

// });

/************************************************************
Menu
************************************************************/

console.log('hello');
var menuItems = document.querySelectorAll('li.menu-item-has-children');
var menuToggle = document.getElementById('header-navigation--toggle');
var menuMobile = document.getElementById('popover-navigation--menu');
var menuMobileItems = document.querySelectorAll('button.popover-navigation--dropdown-toggle');



console.log(menuMobile, menuToggle);

Array.prototype.forEach.call(menuItems, function(el, i){
	el.addEventListener("mouseover", function(event){
		console.log("here noooo");
		this.classList.add("sub-menu-show");
		this.firstElementChild.setAttribute('aria-expanded', "true");
	});
	el.addEventListener("mouseout", function(event){
		document.querySelector(".menu-item-has-children.sub-menu-show").classList.remove('sub-menu-show');
		this.firstElementChild.setAttribute('aria-expanded', "false");
	});
});


// Array.prototype.forEach.call(menuItems, function(el, i){
// 	var activatingA = el.querySelector('a');


Array.prototype.forEach.call(menuMobileItems, function(el, i){

	el.addEventListener("click", function(event){
		console.log("here we are");
		console.log(this.parentNode.classList);
		if (this.parentNode.classList.contains("sub-menu-show--mobile")) {
			this.parentNode.classList.remove("sub-menu-show--mobile");
			this.previousSibling.setAttribute('aria-expanded', "false");
		}
		else {
		console.log("here we are agan x 3");

			this.parentNode.classList.add("sub-menu-show--mobile");
			this.previousSibling.setAttribute('aria-expanded', "true");
		}
	});

});


	// el.querySelector(' popover-navigation--dropdown-toggle').addEventListener("click",  function(event){
	// 	if (this.parentNode.className == "menu-item-has-children") {
	// 		this.parentNode.className = "menu-item-has-children sub-menu-show";
	// 		this.parentNode.querySelector('a').setAttribute('aria-expanded', "true");
	// 		this.parentNode.querySelector('button').setAttribute('aria-expanded', "true");
	// 	} else {
	// 		this.parentNode.className = "menu-item-has-children";
	// 		this.parentNode.querySelector('a').setAttribute('aria-expanded', "false");
	// 		this.parentNode.querySelector('button').setAttribute('aria-expanded', "false");
	// 	}
	// 	event.preventDefault();
	// });


// });


	menuToggle.addEventListener("click",  function(event){
				this.classList.toggle("closed");
				menuMobile.classList.toggle("active");
				if(this.getAttribute("aria-expanded")  == 'true') {
					this.setAttribute('aria-expanded', "false");
				}
				else {
					this.setAttribute('aria-expanded', "true");
				}

	});


});





