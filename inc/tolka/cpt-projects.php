<?php

//CPT
function custom_post_type_project() {
    $labels = array(
        'name'                => _x( 'Project', 'Post Type General Name', 'tolka' ),
        'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'tolka' ),
        'menu_name'           => __( 'Projects', 'tolka' ),
        'parent_item_colon'   => __( 'Parent Project', 'tolka' ),
        'all_items'           => __( 'All Projects', 'tolka' ),
        'view_item'           => __( 'View Project', 'tolka' ),
        'add_new_item'        => __( 'Add New Project', 'tolka' ),
        'add_new'             => __( 'Add New', 'tolka' ),
        'edit_item'           => __( 'Edit Project', 'tolka' ),
        'update_item'         => __( 'Update Project', 'tolka' ),
        'search_items'        => __( 'Search Project', 'tolka' ),
        'not_found'           => __( 'Not Found', 'tolka' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'tolka' ),
    );

$args = array(
	'label'                 => __( 'Project', 'tolka' ),
	'menu_icon'           => 'dashicons-art',
	'description'           => __( 'Project', 'tolka' ),
	'labels'                => $labels,
	'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', 'page-attributes', 'revisions' ,'category'),
	'hierarchical'          => true,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 5,
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => true,
	'can_export'            => true,
	'has_archive'           => true,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'capability_type'       => 'post',
	'taxonomies'          => array('industry'),
	'rewrite' => array(
		'slug' => 'projects'
	)
	 /*// 'rewrite' => array('slug' => '/', 'with_front' => false), IF YOU WANT TO REMOVE THE SLUG FROM THE URL */

);
    register_post_type( 'project', $args );
}

add_action( 'init', 'custom_post_type_project', 0 );

//Taxonomy
function create_industries_hierarchical_taxonomy() {

	$labels = array(
	  'name' => _x( 'Industries', 'taxonomy general name' ),
	  'singular_name' => _x( 'Industry', 'taxonomy singular name' ),
	  'search_items' =>  __( 'Search industries' ),
	  'all_items' => __( 'All industries' ),
	  'parent_item' => __( 'Parent Industry' ),
	  'parent_item_colon' => __( 'Parent Industry:' ),
	  'edit_item' => __( 'Edit Industry' ),
	  'update_item' => __( 'Update Industry' ),
	  'add_new_item' => __( 'Add New Industry' ),
	  'new_item_name' => __( 'New Industry Name' ),
	  'menu_name' => __( 'Industries' ),
	);

	register_taxonomy('industry', array('project'), array(
	  'hierarchical' => true,
	  'labels' => $labels,
	  'show_ui' => true,
	  'show_admin_column' => true,
	  'query_var' => true,
	  'has_archive' => true,
	  'public'       => true,
	  //'rewrite' => array( 'slug' => 'industry' ),
	));
  }

  add_action( 'init', 'create_industries_hierarchical_taxonomy', 0 );
