<?php

function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);        // Activity Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);         // Plugins Widget

	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);    // Quick Press Widget
	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);  // Recent Drafts Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           // WordPress news and events widget
	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);      //

	// remove plugin dashboard boxes
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);           // Yoast's SEO Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);        // Gravity Forms Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);   // bbPress Plugin Widget
}


add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );


// changing the logo link from wordpress.org to your site
function tolka_login_url() {
	return home_url();
}

// changing the alt text on the logo to show your site name
function tolka_login_title() {
	return get_option( 'blogname' );
}



// calling it only on the login page
// add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter( 'login_headerurl', 'tolka_login_url' );
add_filter( 'login_headertitle', 'tolka_login_title' );


function add_admin_footer() {
	echo '<span id="tolka">Website by <a href="https://tolka.io/" target="_blank" style="text-decoration: none;">Tolka</a></span>';
}
add_filter( 'admin_footer_text', 'add_admin_footer' );


add_action('wp_dashboard_setup', 'tolka_dashboard_widgets');

function tolka_dashboard_widgets() {
global $wp_meta_boxes;

wp_add_dashboard_widget('custom_help_widget', 'DGIT Theme Support', 'custom_dashboard_help');
}

function custom_dashboard_help() {
echo '<p>Welcome to the DGIT theme!</p>';
echo '<p>This site has 6 post types</p>';
echo '<ul><li>News</li>';
echo '<li>Perspectives</li>';
echo '<li>Videos</li>';
echo '<li>Case Studies</li>';
echo '<li>Events</li>';
echo '<li>Training Courses</li></ul>';
echo '<p>Featured Images should the following dimensions 352px x 200px</p>';
echo '<p>Extracts should be added for all posts</p>';
}
