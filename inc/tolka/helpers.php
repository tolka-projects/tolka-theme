<?php

//PAGINATION ON CUSTOM QUERIES
if ( ! function_exists( 'pagination' ) ) :
    function pagination( $paged = '', $max_page = '' )
    {
        $big = 999999999; // need an unlikely integer
        if( ! $paged )
            $paged = get_query_var('paged');
        if( ! $max_page )
            $max_page = $wp_query->max_num_pages;

        echo paginate_links( array(
            'base'       => str_replace($big, '%#%', esc_url(get_pagenum_link( $big ))),
            'format'     => '?paged=%#%',
            'current'    => max( 1, $paged ),
            'total'      => $max_page,
            'mid_size'   => 1,
            'prev_text'  => __(''),
            'next_text'  => __(''),
            'type'       => 'plain'
        ) );
    }
endif;


//ADD CUSTOM EXCERPTS TO PAGES FOR SEARCH RESULTS
add_post_type_support( 'page', 'excerpt' );


//REMOVE ARCHIVE PAGES FOR CUSTOM TAXONOMIES - WE ARE USING PAGE TEMPLATES*/
//add_action('template_redirect', 'meks_remove_wp_archives');
// function meks_remove_wp_archives(){
//   if( is_tax()) {
//     global $wp_query;
//     $wp_query->set_404(); //set to 404 not found page
//   }
// }

// REMOVE READ MORE EXCERPT  //
function new_excerpt_more($more) {
    global $post;
    remove_filter('excerpt_more', 'new_excerpt_more');
    return ' ';
  }
  add_filter('excerpt_more','new_excerpt_more',11);


//EXTEND WORDPRESS SEARCH TO INCLUDE PAGES & CTP
  add_filter( 'pre_get_posts', 'custom_post_type_search' );
function custom_post_type_search( $query ) {
     if ($query->is_search) {
          $query->set('post_type', array( 'post', 'page', 'battery'));
     }
     return $query;
}


//EXTEND WORDPRESS SEARCH TO INCLUDE ACF FIELDS
/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

//REMOVE P TAGS FROM AROUND IMAGES
function bones_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}


//RENAME POSTS TO NEWS
add_action( 'init', 'tolka_change_post_object' );
// Change dashboard Posts to News
function tolka_change_post_object() {
    $get_post_type = get_post_type_object('post');
    $labels = $get_post_type->labels;
        $labels->name = 'News';
        $labels->singular_name = 'News';
        $labels->add_new = 'Add News';
        $labels->add_new_item = 'Add News';
        $labels->edit_item = 'Edit News';
        $labels->new_item = 'News';
        $labels->view_item = 'View News';
        $labels->search_items = 'Search News';
        $labels->not_found = 'No News found';
        $labels->not_found_in_trash = 'No News found in Trash';
        $labels->all_items = 'All News';
        $labels->menu_name = 'News';
        $labels->name_admin_bar = 'News';
}



if ( ! function_exists( 'tolka_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function tolka_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( 'd M Y' ) ),
			esc_html( get_the_date( 'd M Y') ),
		);

		$posted_on   = apply_filters(
			'tolka_posted_on',
			sprintf(
				'<span class="posted-on" itemprop="datePublished">%1s</span>',
				 $time_string
			)
		);
		echo $posted_on; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}


if ( ! function_exists( 'tolka_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function tolka_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="navigation post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'tolka' ); ?></h2>
			<div class="nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<span class="nav-previous pr-6 read-more back">%link</span>', _x( '<img class="pr-3 mr-1" src="/wp-content/themes/dgit/assets/icons/icon-post-arrow-back.svg">BACK', 'Previous post link', 'tolka' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<span class="nav-next read-more">%link</span>', _x( 'NEXT<img class="pl-3 ml-1" src="/wp-content/themes/dgit/assets/icons/icon-post-arrow-next.svg">', 'Next post link', 'tolka' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}


if ( ! function_exists( 'tolka_post_nav_sidebar' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function tolka_post_nav_sidebar() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="navigation post-navigation mb-2">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'tolka' ); ?></h2>
			<div class="nav-links justify-content-between">
				<?php
				if ( get_next_post_link() ) {
					next_post_link( '<div class="pb-2"><span class="nav-next read-more">%link</span></div>', _x( 'Next &#8594;', 'Next post link', 'tolka' ) );
				}
				if ( get_previous_post_link() ) {
					previous_post_link( '<div><span class="nav-previous pr-6 read-more back pb-2">%link</span></div>', _x( '&#8592; Previous', 'Previous post link', 'tolka' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}
