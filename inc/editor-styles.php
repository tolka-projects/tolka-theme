<?php
/**
 * tolka modify editor
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


//Add custom styles dropdown in WYSIWYG

function tolka_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'tolka_mce_buttons_2');

//Add custom stylesheet to Editor
/**
 * Registers an editor stylesheet for the theme.
 */
function tolka_add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/assets/css/custom-editor-style.min.css' );
}
add_action( 'admin_init', 'tolka_add_editor_styles' );

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

    $style_formats = array(
		array(
            'title' => 'Section Title Three',
            'block' => 'span',
            'classes' => '.text--title-section-three',
            'wrapper' => true,
        ),
        array(
            'title' => 'Section Title Two',
            'block' => 'span',
            'classes' => '.text--title-section-two',
            'wrapper' => true,
        ),
		array(
            'title' => 'Section Title',
            'block' => 'span',
            'classes' => '.text--title-section',
            'wrapper' => true,
        ),
        array(
            'title' => 'Small Caps',
            'block' => 'span',
            'classes' => 'text--caps-small ',
            'wrapper' => true,
		),
		array(
			'title' => 'Body',
			'block' => 'span',
			'classes' => 'text--body ',
			'wrapper' => true,
		),
		array(
			'title' => 'Intro',
			'block' => 'span',
			'classes' => 'text--intro',
			'wrapper' => true,
		),

    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function changeMceDefaults($in) {

    $in[ 'wordpress_adv_hidden' ] = FALSE;
    return $in;
}
add_filter( 'tiny_mce_before_init', 'changeMceDefaults' );


function my_mce4_options($init) {

    $custom_colours = '
        "33ACE3", "Blue Light",
        "243746", "Blue Dark",
        "1A4684", "Blue Mid",
        "ffffff", "White",
        "000000", "Black",
        "ffffff", "Color 5 name",
        "ffffff", "Color 6 name",
        "ffffff", "Color 7 name"
    ';

    // build colour grid default+custom colors
    $init['textcolor_map'] = '['.$custom_colours.']';

    // change the number of rows in the grid if the number of colors changes
    // 8 swatches per row
    $init['textcolor_rows'] = 1;

    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');


add_filter( 'tiny_mce_before_init', 'tolka_tiny_mce_before_init' );

if ( ! function_exists( 'tolka_tiny_mce_before_init' ) ) {
	/**
	 * Adds style options to TinyMCE's Style dropdown.
	 *
	 * @param array $settings TinyMCE settings array.
	 * @return array
	 */
	function tolka_tiny_mce_before_init( $settings ) {

		$style_formats = array(
			array(
				'title'    => 'Lead Paragraph',
				'selector' => 'p',
				'classes'  => 'lead',
				'wrapper'  => true,
			),
			array(
				'title'  => 'Small',
				'inline' => 'small',
			),
			array(
				'title'   => 'Blockquote',
				'block'   => 'blockquote',
				'classes' => 'blockquote',
				'wrapper' => true,
			),
			array(
				'title'   => 'Blockquote Footer',
				'block'   => 'footer',
				'classes' => 'blockquote-footer',
				'wrapper' => true,
			),
			array(
				'title'  => 'Cite',
				'inline' => 'cite',
			),
		);

		if ( isset( $settings['style_formats'] ) ) {
			$orig_style_formats = json_decode( $settings['style_formats'], true );
			$style_formats      = array_merge( $orig_style_formats, $style_formats );
		}

		$settings['style_formats'] = wp_json_encode( $style_formats );
		return $settings;
	}
}
