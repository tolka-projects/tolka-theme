<?php

/*
* Template Name: Swiper Template
/*
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site will use a
* different template.
*
* @package tolka
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<main class="site-main py-md-8 py-5" id="main" role="main">



<section class="position-relative">
	<div class="swiper-container">
        <div class="swiper-wrapper">
		<?php if( have_rows('slide') ):
				while ( have_rows('slide') ) : the_row(); ?>
					<?php $hero = get_sub_field('image') ?>
            		<div class="swiper-slide" style="background-image: url(<?php echo esc_url( $hero['url'] ); ?>)"></div>
				<?php endwhile; ?>
		<?php endif; ?>
		</div>
	</div>
	<div class="swiper-button-prev"></div>
	<div class="swiper-button-next"></div>
</section>

</main>

<?php get_footer() ?>

<script>
const swiper = new Swiper('.swiper-container', {
	// Optional parameters
	speed: 500,
	direction: 'horizontal',
	loop: true,
	slidesPerView: 3,
	spaceBetween: 25,
	autoplay: {
	delay: 1000,
	},
	breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    // when window width is >= 480px
    480: {
      slidesPerView: 2,
      spaceBetween: 30
    },
    // when window width is >= 640px
    640: {
      slidesPerView: 3,
      spaceBetween: 40
    }
  },
	navigation: {
	nextEl: '.swiper-button-next',
	prevEl: '.swiper-button-prev',
	},
	});

</script>
