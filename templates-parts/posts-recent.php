<?php

$args = array(
	'posts_per_page' => 3,
);
$featured = new WP_Query($args); ?>

<div class="container">
<div class="row">
	<?php if ($featured->have_posts()):
	while($featured->have_posts()):
	$featured->the_post();
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
	?>

	<div class="col-12 col-md-4 col-lg-4 mb-5 mb-md-0">
		<a href="<?php the_permalink(); ?>">
		<h3 class="mb-2"> <?php the_title(); ?></h3>
		<p class="mb-2">READ MORE</p>
		<img src="<?php echo $featured_img_url ?>" class="img-fluid">
		</a>
	</div>
<?php

endwhile;
endif; ?>
</div>
</div>
<?php wp_reset_postdata();
?>
