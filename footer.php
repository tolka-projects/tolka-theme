<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


?>
<?php if(  class_exists('ACF') ) : ?>
	<?php $footer_logo = get_field("footer_logo" ,"option") ?>
	<?php $copy = get_field("copyright_notice" ,"option") ?>
<?php endif ?>

<footer id="site-footer" class="" itemscope itemtype="http://schema.org/WPFooter">

	<div id="footer-top" class="footer-top py-5">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3">
					<div class="footer-top--logo">
						<?php if($footer_logo): ?>
							<a href="/home"><img src="<?php echo $footer_logo['url'] ?>" alt="<?php echo $footer_logo['alt'] ?>" ></a>
						<?php else: ?>
							<a href="/home"><img src="<?php echo get_template_directory_uri() ?>/assets/icons/lightning-bolt.svg" class="img-fluid" alt="tolka-theme-logo"></a>
						<?php endif ?>
					</div>
					<h2>Tolka Web Development</h2>
				</div>
				<div class="col-12 col-md-3">
					<div><?php wp_nav_menu(array(
						'menu' => 'footer-menu',
						'container_class' => 'footer-navigation--container',
						'menu_class' => 'footer-navigation--menu list-unstyled mb-0',
						 )); ?>
					</div>
				</div>
				<div class="col-12 col-md-3">
					<div><?php wp_nav_menu(array(
						'menu' => 'footer-menu',
						'container_class' => 'footer-navigation--container',
						'menu_class' => 'footer-navigation--menu list-unstyled mb-0',
						 )); ?>
					</div>
				</div>
				<div class="col-12 col-md-3">
					<div><?php wp_nav_menu(array(
						'menu' => 'footer-menu',
						'container_class' => 'footer-navigation--container',
						'menu_class' => 'footer-navigation--menu list-unstyled mb-0',
						 )); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="footer-bottom" class="footer-bottom bg-color--color-feature">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				Copyright &copy; <?php echo date("Y") ?>  <?php echo $copy ?> - Website built by <a href="https://tolka.io" target="_blank" title="tolka web development">Tolka</a>
				</div>
			</div>
		</div>
	</div>

	</div>

</footer><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->
<!-- RENDER SEARCH MODAL IF REQUIRED -->
<?php render_search_modal() ?>
<?php wp_footer(); ?>

</body>

</html>

