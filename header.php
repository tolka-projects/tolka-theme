<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php if(get_field('header', 'options')) :?>
		<?php the_field('header', 'options')?>
	<?php endif ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">


	<?php wp_head(); ?>
</head>

<body <?php body_class("no-js"); ?> <?php tolka_body_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">

<?php if(get_field('body', 'options')) :?>
	<?php the_field('body', 'options')?>
<?php endif ?>

<?php do_action( 'wp_body_open' ); ?>


<div class="site" id="page">
<header class="header print-hide " id="site-header" itemscope itemtype="http://schema.org/WPHeader">
	<div class="container position-relative">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" class="header-navigation">

		<nav id="main-nav" class="header-navigation--nav d-flex" aria-labelledby="main-nav-label" itemscope itemtype="http://schema.org/SiteNavigationElement">

			<h2 id="main-nav-label" class="visually-hidden">
				<?php esc_html_e( 'Main Navigation', 'tolka' ); ?>
			</h2>

			<a class="visually-hidden-focusable" href="#content">Skip to main content</a>

			<?php $logo = get_field('header_logo', 'options') ?>
			<div class="header-navigation--logo position-relative">
				<a href="<?php echo get_home_url(); ?>" class="custom-logo-link" rel="home" title="<?php echo $logo['alt']?>">
				<?php if($logo): ?>
					<img src="<?php echo $logo['url']?>" class="img-fluid" alt="<?php echo $logo['alt']?>">
				<?php else: ?>
					<img src="<?php echo get_template_directory_uri() ?>/assets/icons/lightning-bolt.svg" class="img-fluid" alt="tolka-theme-logo">
				<?php endif ?>
				</a>
			</div>

			<!-- RENDER THE SEARCH TRIGGER ICON IF REQUIRED -->
			<?php /*  render_search_trigger()  */?>
			<div class="d-flex align-items-center">
				<button id="header-navigation--toggle" class="header-navigation--toggle closed" type="button" data-toggle="collapse" data-target="#popover-navigation--menu" aria-controls="#popover-navigation--menu" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'tolka' ); ?>">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</button>
			</div>
			<div class="popover-navigation--menu" id="popover-navigation--menu">
			<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'header-navigation--container',
						'container_id'    => '',
						'menu_class'      => 'header-navigation--menu d-flex flex-column flex-md-row list-unstyled mb-0',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new bootstrap_5_wp_nav_menu_walker(),
					)
				);
				?>
			</div>


			</nav><!-- .site-navigation -->

		</div><!-- .container -->
	</div><!-- #wrapper-navbar end -->



</header>


