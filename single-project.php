<?php
/**
 * The template for displaying all CPT Projects
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>


	<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container" tabindex="-1">
			<div class="row">
				<div class="col-12">


				<?php
				while ( have_posts() ) {
					the_post();
					get_template_part( 'templates-loop/content', 'single-project' );
					tolka_post_nav();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				}
				?>
				</div><!-- .col-12 -->
			</div><!-- .row -->
		</div><!-- #content -->
	</main><!-- #main -->

<?php
get_footer();
