<?php
/**
 * tolka functions and definitions
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/*** Project & Client Details - these must match css variables for Gutenberg styles to work ***/
define('theme_colours',  [
	"Primary" => "#1a6c7a",
	"Secondary" => "#9C9657",
	"Text" => "#153243",
	"White" => "#ffffff",
	"Black" => "#000000",
]
);



$tolka_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	//'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	//'/customizer.php',                      // Customizer additions.
    // '/custom-comments.php',                 // Custom Comments file.
	// '/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/tolka/tolka/issues/567.
	'/class-wp-bootstrap5-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/tolka/tolka/issues/567.

	'/class-wp-tolka-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/tolka/tolka/issues/567.
	// '/woocommerce.php',                     // Load WooCommerce functions.
	'/editor-styles.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
	'/gutenberg.php',                      // Load deprecated functions.

);

foreach ( $tolka_includes as $file ) {
	require_once get_template_directory() . '/inc' . $file;
}

if(in_array('advanced-custom-fields-pro/acf.php', apply_filters('active_plugins', get_option('active_plugins')))){
	require 'inc/tolka/acf.php'; // ACF OPTIONS PAGE
}

require 'inc/tolka/admin-area.php'; // CUSTOMISE ADMIN AREA - TURN ON/OFF AS REQUIRED
require 'inc/tolka/search-trigger.php'; // SEARCH MODAL
require 'inc/tolka/cpt-projects.php'; //CUSTOM POST TYPE
require 'inc/tolka/cpt-events.php'; //CUSTOM POST TYPE
require 'inc/shortcodes/social-shares.php'; //Social Shares on Single posts



require 'inc/tolka/helpers.php'; //
